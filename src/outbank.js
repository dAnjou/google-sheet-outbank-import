function toast(thing) {
  SpreadsheetApp.getActive().toast(thing);
}

function onOpen() {
  var menu = SpreadsheetApp.getUi().createMenu('Manage');
  menu.addItem('Import from Outbank', 'importFromOutbankDialog').addToUi();
  //toast(folderId);
}

function importFromOutbankDialog() {
  var template = HtmlService.createTemplateFromFile('import');
  var sheet = SpreadsheetApp.getActive().getSheetByName("Sum");
  var folderId = sheet.getRange(1, 14).getValue();
  template.files = getFiles(folderId);
  SpreadsheetApp.getUi().showModalDialog(template.evaluate(), 'Import from Outbank');
}

function getFiles(folderId) {
  var folder = DriveApp.getFolderById(folderId);
  var fileIterator = folder.getFiles();
  var files = [];
  while (fileIterator.hasNext()) {
    files.push(fileIterator.next());
  }
  return files;
}

function importCSV(fileId) {
  var file = DriveApp.getFileById(fileId);
  var spreadsheet = SpreadsheetApp.getActive();
  var sheet = spreadsheet.insertSheet(file.getName(), spreadsheet.getNumSheets());
  var content = file.getBlob().getDataAsString();
  var data = Utilities.parseCsv(content, ";");
  data.shift();
  sheet.getRange(1, 1, data.length, data[0].length).setValues(data);
  sheet.insertColumnAfter(4);
  data.forEach(function (row, index) {
    var rowNumber = index + 1;
    var date = row[2].split(".");
    sheet.getRange(rowNumber, 3)
      .setValue(`${date[2]}-${date[1]}-${date[0]}`)
      .setNumberFormat("yyyy-mm-dd");
    sheet.getRange(rowNumber, 4)
      .setFormula(`MONTH(C${rowNumber})`);
    sheet.getRange(rowNumber, 5)
      .setFormula(`YEAR(C${rowNumber})`);
    sheet.getRange(rowNumber, 6)
      .setValue(row[4].replace(",", "."))
      .setNumberFormat("[$€]#,##0.00");
    var target = spreadsheet.getSheetByName(row[1]);
    sheet.getRange(rowNumber, 1, 1, sheet.getLastColumn())
      .copyTo(target.getRange(target.getLastRow() + 1, 1, 1, target.getLastColumn()));
    toast(`${rowNumber} / ${data.length}`);
  });
  toast("Done!");
};
